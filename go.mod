module gitlab.com/cabzy/policyserviceclient.git

go 1.14

require (
	gitlab.com/cabzy/x.git v0.0.0-20200501122102-c4bc476fad62
	go.uber.org/fx v1.12.0
)
