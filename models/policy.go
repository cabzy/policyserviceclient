package models

type PolicyForm struct {
	Subject  string `json:"subject,string" form:"subject,string"`
	Tenant   string `json:"tenant,string" form:"tenant,string"`
	Resource string `json:"resource,string" form:"resource,string"`
	Action   string `json:"action,string" form:"action,string"`
}

func (f *PolicyForm) IsValid() bool {
	return !(f.Subject == "" || f.Tenant == "" || f.Resource == "" || f.Action == "")
}
